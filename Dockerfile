FROM node:8

WORKDIR /opt/

RUN ["apt","install","git"]
RUN ["git","clone","https://gitlab.com/alan-drieux/yu-gi-market.git"]

WORKDIR /opt/yu-gi-market

RUN [ "npm", "install" ]

EXPOSE 5000

RUN [ "npm", "run", "start" ]

RUN echo "serveur lancer"