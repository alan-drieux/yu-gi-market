const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Cards
router.get('/', async (req,res)=>{
    const cards = await loadCardsCollection();
    res.send(await cards.find({}).toArray());
})

// Add Cards
router.post('/', async (req,res) => {
    const cards = await loadCardsCollection();
    await cards.insertOne({
        cardName: req.body.cardName,
        cardEffect: req.body.cardEffect,
        cardAttack: req.body.cardAttack,
        cardDefense: req.body.cardDefense,
        cardImage: req.body.cardImage,
        addedAt: new Date()
    });
    res.status(201).send();
})


// Deleted Cards
router.delete('/:id', async(req,res)=>{
    const cards = await loadCardsCollection();
    await cards.deleteOne({_id:new mongodb.ObjectID(req.params.id)});
    res.status(200).send();
});

async function loadCardsCollection(){
    const client = await mongodb.MongoClient.connect('mongodb://vikmanator:99917440Abc@ds013300.mlab.com:13300/yu-gi-market-cards', {
        useNewUrlParser:true
    });

    return client.db('yu-gi-market-cards').collection('cards');
}

// Exports the router
module.exports = router;