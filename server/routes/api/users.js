const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Users
router.get('/', async (req,res)=>{
    const users = await loadUsersCollection();
    res.send(await users.find({}).toArray());
})

// Add Users
router.post('/', async (req,res) => {
    const users = await loadUsersCollection();
    await users.insertOne({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        name: req.body.name,
        surname:req.body.surname,
        age:req.body.age,
        accountCreatedAt: new Date()
    });
    res.status(201).send();
})


// Deleted Users
router.delete('/:id', async(req,res)=>{
    const users = await loadUsersCollection();
    await users.deleteOne({_id:new mongodb.ObjectID(req.params.id)});
    res.status(200).send();
});

async function loadUsersCollection(){
    const client = await mongodb.MongoClient.connect('mongodb://vikmanator:99917440Abc@ds013300.mlab.com:13300/yu-gi-market-cards', {
        useNewUrlParser:true
    });

    return client.db('yu-gi-market-cards').collection('users');
}

// Exports the router
module.exports = router;