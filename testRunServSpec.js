var assert = require("assert")
const {
    spawn
} = require("child_process")
let tcpp = require('tcp-ping');

let grep = spawn('node', ["server/index.js"]);
console.log(grep.pid)

setTimeout(() => {
    tcpp.probe('127.0.0.1', 5000, function (err, available) {
        console.log(available)
        assert.strictEqual(true, available);
    });
    grep.kill()
}, 1000)