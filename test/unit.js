//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

// let mongoose = require("mongoose");
// let Book = require('../app/models/book');

const mongodb = require('mongodb');

async function loadCardsCollection() {
    const client = await mongodb.MongoClient.connect('mongodb://vikmanator:99917440Abc@ds013300.mlab.com:13300/yu-gi-market-cards', {
        useNewUrlParser: true
    });

    return client.db('yu-gi-market-cards').collection('cards');
}

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server/index');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Card', () => {
    /*
     * Test the /GET route
     */
    describe('/GET card', () => {
        it('it should GET all the cards', (done) => {
            chai.request(server)
                .get('/api/cards')
                .end((err, res) => {
                    console.log(res)
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.above(0);
                    done();
                });
            setTimeout(() => {
                process.exit()
            }, 1000)
        });
    });

});