# yugi-shop-API

[![pipeline status](https://gitlab.com/alan-drieux/yu-gi-market/badges/master/pipeline.svg)](https://gitlab.com/alan-drieux/yu-gi-market/commits/master)

## 🃏 Welcolme to the new Yu-Gi-Shop REST API

Here you can edit the API, add features such as User Authentification...
This API is made with Express and use mangodb.

## ⛏️ Project setup
```
npm install
```

### ⚗️ Compiles and hot-reloads for development
```
npm run dev
```

