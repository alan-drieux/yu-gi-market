# yugi-shop

## 🃏 Welcolme to the new Yu-Gi-Oh card Shop

You will be able to trade your cards or search for new !

## ⛏️ Project setup
```
npm install
```

### ⚗️ Compiles and hot-reloads for development
```
npm run serve
```

### 🔥 Compiles and minifies for production
```
npm run build
```

### 🏃 Run your tests
```
npm run test
```

### 💔 Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
