import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify, {
    theme: {
        primary: '#dd0808',
        primaryContrast: '#ffffff',
        secondary: '#424242',
        button: '#8fceff',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
    },
    darkTheme: { background: '#2e1a6c', }
})
