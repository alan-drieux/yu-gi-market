import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home'
import SignUp from './components/SignUp'
import LandingPage from './components/LandingPage'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/signUp',
            name: 'signUpComp',
            component: SignUp
        },
        {
            path: '/',
            name: 'landingPageComp',
            component: LandingPage
        }
    ],
    beforeResolve: ((to, from, next) => {
        /* must call `next` */
        next
    }),
});