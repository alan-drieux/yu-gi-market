import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

// In the state we will store our data, you can import it by importing the store into your component.
Vue.use(Vuex)
export const store = new Vuex.Store({
    strict: true,
    state: {
        users: {
            userInfo: []
        },
        cards: {
            cardList: []
        }

    },

    mutations: {
        setCardList(state, cardList) {
            state.cards.cardList = cardList;
        }
    },
    actions: {
        getcards: function ({commit}) {
            axios
                .get("http://localhost:5000/api/cards")
                .then(response => response.data)
                .then(cardList =>{
                    commit('setCardList', cardList)
                })
                .catch(function (error) {
                    console.log(error);
                });
            
        }
    },
    getters: {
        cardList(state){
            return state.cards.cardList;
        }
    }
})